import java.io.IOException;
import java.util.Collections;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;

/**
 * @author piumnl
 * @version 1.0.0
 * @since on 2017-12-16.
 */
public class TransferMain {

    public static void main(String[] args) {
        Options options = parseParam();
        String format = "[-p password] [-u username] [-P scp port] [-h target ip] [-s source file] [-t target file]";

        HelpFormatter helpFormatter = new HelpFormatter();
        DefaultParser defaultParser = new DefaultParser();
        CommandLine parse;
        try {
            int port = 22;
            String password = null;
            String sourceFile;
            String targetIP;
            String targetUser;
            String targetFile;

            parse = defaultParser.parse(options, args);
            if (parse.hasOption('P')) {
                port = Integer.parseInt(parse.getOptionValue('P'));
            }
            if (parse.hasOption('p')) {
                password = parse.getOptionValue('p');
            }
            if (parse.hasOption('s')) {
                sourceFile = parse.getOptionValue('s');
            } else {
                throw new MissingOptionException(Collections.singletonList(options.getOption("s")));
            }
            if (parse.hasOption('h')) {
                targetIP = parse.getOptionValue('h');
            } else {
                throw new MissingOptionException(Collections.singletonList(options.getOption("h")));
            }
            if (parse.hasOption('u')) {
                targetUser = parse.getOptionValue('u');
            } else {
                throw new MissingOptionException(Collections.singletonList(options.getOption("u")));
            }
            if (parse.hasOption('t')) {
                targetFile = parse.getOptionValue('t');
            } else {
                throw new MissingOptionException(Collections.singletonList(options.getOption("t")));
            }

            // 文件 scp 到数据服务器
            transferFile(sourceFile, targetFile, targetIP, port, targetUser, password);
        } catch (MissingOptionException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            helpFormatter.printHelp(format, options);
        }
    }

    private static Options parseParam() {
        Options options = new Options();
        options.addOption("P", true, "指定目标机器的端口");
        options.addOption("h", true, "指定目标机器的IP");
        options.addOption("u", true, "指定登录目标机器的用户");
        options.addOption("p", true, "指定登录目标机器的密码");
        options.addOption("s", true, "指定要传输的文件路径");
        options.addOption("t", true, "指定文件传输到目标机器中的路径");

        return options;
    }

    private static void transferFile(String sourceFile, String targetFile, String targetIP, int port, String targetUser, String targetPassword) {
        Connection conn = new Connection(targetIP, port);
        try {
            conn.connect();
            boolean isAuthenticated = conn.authenticateWithPassword(targetUser, targetPassword);
            if (!isAuthenticated) {
                throw new IOException("Authentication failed. 文件 scp 到数据服务器时发生异常");
            }
            SCPClient client = new SCPClient(conn);
            client.put(sourceFile, targetFile);
            conn.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
